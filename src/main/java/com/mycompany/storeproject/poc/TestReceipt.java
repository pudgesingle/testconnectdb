/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author PlugPC
 */
public class TestReceipt {
    public static void main(String[] args){
        Product p1 = new Product(1, "Chusdf", 30);
        Product p2 = new Product(2, "Kedr", 50);
        User seller = new User("Gege", "0888888888", "1234");
        Customer customer = new Customer("Nehdsee", "0222222222");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 5);
        receipt.addReceiptDetail(p2, 4);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        receipt.addReceiptDetail(p1, 4);
        System.out.println(receipt);
        
    }
}
